# Textboxes

A plugin for [CKEditor5 Embedded Content](https://www.drupal.org/project/ckeditor5_embedded_content) for Drupal 9/10.

## Summary

My design had divs for articles where text-box and color names will provide thick bordered boxes with content in it.

This code is actually a derivative of one of the examples tailored to the need and have very narrow application. It offers four different background options (none, orange, gray, black) and add a class name to the div accordingly. It should be easy enough for anyone to change/add/remove the classes from the output. The main advantage of having this feature as a plugin of the module is that you can edit the content later (unlike the images embedded).

## How it works?

I didn't check how it works if the module is not installed, but if it's already there and this custom module is installed and enabled, it adds a new option called Textboxes to the Embedded Content button in CKEditor5. How to add that button is the main module's job (through the text filters).

My scenario needed a class called text-box and optionally color, so I have created the template with this class and the option. You may edit it for your needs.
